import Input from './components/Input'
import PersonList from './components/PersonList.js';
import './App.css';
import React,{useState} from 'react';

function App() {
  const [token, setToken] = useState('')


  return (
    <div>
      <Input onSubmit={(value) => setToken(value)} />

      <div>
        <PersonList token={token}/>
      </div>
    </div>
  );
}




export default App;
