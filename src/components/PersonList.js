import React from "react";
import axios from "axios";

export default class PersonList extends React.Component {
  state = {
    persons: [],
  };

  componentDidMount() {
    // axios.get(`https://jsonplaceholder.typicode.com/users`)
    // axios.get(`https://gitlab.com/api/v4/projects`)
  }

  componentDidUpdate(prevprops) {
    if (this.props.token !== prevprops.token) {
      axios
        .get(`https://gitlab.com/api/v4/users`, {
          headers: {
            Authorization: `Bearer ${this.props.token}`,
          },
        })

        .then((res) => {
          const persons = res.data;
          this.setState({ persons });
        });
    }
  }

  // token code.....
  // glpat-nnWT-12yxnFvzqzzpe5M

  render() {
    return (
      <ul>
        {this.state.persons.map((person) => (
          <li key={person.id}>{person.name}</li>
        ))}
      </ul>
    );
  }
}
