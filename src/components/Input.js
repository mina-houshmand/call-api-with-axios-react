import React, { Component } from "react";
export const MContext = React.createContext();

class Input extends Component {
  state = {
    value: "",
  };

  //get value every time we type
  getValue = (event) => {
    // console.log(event.target.value);

    this.setState({ value: event.target.value });
  };

  //get value when click button
  handleSubmit = (event) => {
    event.preventDefault(); //stop reloading
    const value = this.state.value;
    this.props.onSubmit(value);
    console.log(value);
  };

  render() {
    return (
      <div className="wrapper">
        <h1>please insert your token code:</h1>
        <form>
          <fieldset>
            <label>
              <p>token code:</p>
              <input placeholder="insert code" onChange={this.getValue} />
              <button type="submit" onClick={this.handleSubmit}>
                Submit
              </button>
            </label>
          </fieldset>
        </form>
      </div>
    );
  }
  updateInputValue(evt) {
    const val = evt.target.value;
    this.setState({
      // inputValue: updatedInputValue
    });
  }
}

export default Input;
